from common.json import ModelEncoder #watch - did quick fix
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]

class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name"
    ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "url",
        "location",
        "id"
    ]

    encoders = {
        "location": LocationVOEncoder(),
    }
