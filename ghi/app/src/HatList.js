import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function HatList() {
    const [hats, setHats] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    };

    const deleteHat = async (id) => {
        const url = `http://localhost:8090/api/hats/${id}/`
        const fetchConfig = {
            method: "delete"
        }

        const response = await fetch(url,fetchConfig)
        if (response.ok){
            fetchData()
        }
    }

    useEffect(() => {
        fetchData();
    }, [])



    return (
        <div>
            <div className="mt-3">
                <h1 className="align-bottom px-0 align-start form-check form-check-inline">Hats List</h1>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Picture</th>
                        <th>Style</th>
                        <th>Color</th>
                        <th>Fabric</th>
                        <th>Location</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map(hat => (
                        <tr key={hat.id}>
                            <td className="align-middle"><img alt='hat image' src={hat.url} width="50" /></td>
                            <td className="align-middle">{hat.style_name}</td>
                            <td className="align-middle">{hat.color}</td>
                            <td className="align-middle">{hat.fabric}</td>
                            <td className="align-middle">Closet {hat.location.closet_name}, Section {hat.location.section_number}, Shelf {hat.location.shelf_number}</td>
                            <td><button onClick={() => {deleteHat(hat.id)}} className="btn btn-danger">Delete</button></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>

    )
                    }
export default HatList;
