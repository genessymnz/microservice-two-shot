import React, { useState, useEffect } from 'react';

export default function HatForm() {
    const [locations, setLocations] = useState([]);
    const [formData, setFormData] = useState({
        style_name: '',
        color: '',
        fabric: '',
        url: '',
        location: '',
    })


    const handleSubmit = async (event) => {
    event.preventDefault()

    const url = `http://localhost:8090/api/hats/`

    const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
            "Content-Type": "application/json"
        }
    }
    const response = await fetch(url, fetchConfig)
    if (response.ok) {
        const data = await response.json()
        console.log(data)

        setFormData({
            style_name: '',
            color: '',
            fabric: '',
            url: '',
            location: '',
        })
    }
    }
    const handleFormChange = (event) => {
        const value = event.target.value
        const name = event.target.name

        setFormData({
            ...formData,
            [name]: value
        })
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/"
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
        }
    }
    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleSubmit}>
                        <div className="text-center mb-3">
                            <span className="fs-3">Create a hat</span>
                            </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.style_name} name="style_name" id="style_name" placeholder="style_name" />
                            <label htmlFor="style_name">Style Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.fabric} name="fabric" id="fabric" placeholder="fabric" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.color} name="color" id="color" placeholder="Color" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.url} name="url" id="url" placeholder="url" />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select value={formData.location} onChange={handleFormChange} className="form-select" name="location" id="location">
                                <option defaultValue="">--Choose A Closet--</option>
                                {locations.map(location => {
                                    return (<option key={location.href} value={location.href}>{location.closet_name}</option>)
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create!</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
